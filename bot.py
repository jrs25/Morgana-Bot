import discord
import asyncio

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    def is_me(m):
        return m.author == client.user

    if message.content.startswith('*ping'):
        await client.send_message(message.channel, 'Bot is online!')
    if message.content.startswith('*clearbotmsgs'):
        deleted = await client.purge_from(message.channel, limit=100, check=is_me)
        await client.send_message(message.channel, 'Deleted {} message(s)'.format(len(deleted)))
    if message.content.startswith('*help'):
        help = discord.Embed(title='MorganaBot Help', description='''*clearbotmsgs - Clears the bots messages
*ping - Checks if the bot is online
*deletthis - Clears 100 messages
*bed - Just a morgana quote
*notacat - Another Morgana quote''', colour=0x00b9ff)
        help.set_author(name='MorganaBot', icon_url=client.user.default_avatar_url)
        await client.send_message(message.channel, embed=help)
    if message.content.startswith('*bed'):
        await client.send_message(message.channel, "Aren't you tired? You should go to bed.")
    if message.content.startswith('*notacat'):
        await client.send_message(message.channel, "I'm not a cat. *meow*")
    if message.content.startswith('*deletthis'):
        deleted = await client.purge_from(message.channel, limit=100)
        await client.send_message(message.channel, 'Deleted {} message(s)'.format(len(deleted)))
        
client.run('no token here')
